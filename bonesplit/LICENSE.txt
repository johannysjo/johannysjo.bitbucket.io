Copyright (c) 2021 Johan Nysjö

BoneSplit is free to use for research and non-commercial purposes. If
you publish results that have been obtained with the software, please
include a bibliographical reference to the paper below:

  Johan Nysjö, Filip Malmberg, Ida-Maria Sintorn, and Ingela Nyström.
  BoneSplit - A 3D Texture Painting Tool for Interactive Bone
  Separation in CT Images. Journal of WSCG, Vol.23, No.2, pp. 157-166,
  2015.

The software is not approved for clinical use. For commercial use, a
commercial license must be requested by contacting the developer.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
